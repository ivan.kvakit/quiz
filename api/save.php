<?php

    require_once dirname( __DIR__ ) . '/wp-load.php';
    require_once dirname( __DIR__ ) . '/helpers/QuizHelper.php';
    require dirname( __DIR__ ) . '/api/vendor/autoload.php';

    use Dompdf\Dompdf;

    error_reporting(E_ALL);
    ini_set("display_errors", 1);

    QuizHelper::checkTables();

    if(isset($_REQUEST['quiz'])){
        $data = [];
        $data['first_name'] = isset($_REQUEST['quiz']['firstName']) ? $_REQUEST['quiz']['firstName'] : '';
        $data['last_name'] = isset($_REQUEST['quiz']['lastName']) ? $_REQUEST['quiz']['lastName'] : '';
        $data['email'] = isset($_REQUEST['quiz']['email']) ? $_REQUEST['quiz']['email'] : '';
        $data['using_insight'] = isset($_REQUEST['quiz']['howLong']) ? $_REQUEST['quiz']['howLong'] : '';
        $data['point'] = isset($_REQUEST['quiz']['points']) ? $_REQUEST['quiz']['points'] : '';
        $data['likes_about_insight'] = isset($_REQUEST['quiz']['likes']) ? $_REQUEST['quiz']['likes'] : '';
        $data['dislikes_about_insight'] = isset($_REQUEST['quiz']['dislikes']) ? $_REQUEST['quiz']['dislikes'] : '';
        $data['enhancements_insight'] = isset($_REQUEST['quiz']['likeToSee']) ? $_REQUEST['quiz']['likeToSee'] : '';

        $results = [];

        if(isset($_REQUEST['quiz']['area'])){
            if(isset($_REQUEST['quiz']['area']['first'])){
                $results['areas_of_strenght'] = $_REQUEST['quiz']['area']['first'];
            }
            if(isset($_REQUEST['quiz']['area']['second'])){
                $results['areas_of_development'] = $_REQUEST['quiz']['area']['second'];
            }
        }else{
            $results['areas_of_development'] = [];
            $results['areas_of_strenght'] = [];
        }

        $data['results'] = json_encode($results);
        $hash = md5(implode($data));
        $data['hash'] = $hash;

        $dompdf = new Dompdf(array('enable_remote' => true));
        QuizHelper::save($data);
        // $dompdf->loadHtml('<html><body>hello world</body></html>');
        $dompdf->loadHtml(
            file_get_contents('https://insightignite.cbn.com.au/?pdf=1&hash=' . $hash)
        );
        // $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $output = $dompdf->output();
        file_put_contents(dirname( __DIR__ ) . '/api/pdf/'.$hash.'.pdf', $output);
        // echo $hash;
        // $res = shell_exec(dirname( __DIR__ ) . '/helpers/phantom '.dirname( __DIR__ ) .'/helpers/topdf.js '.QuizHelper::HOST.' ' . $hash);

        $to          = $data['email']; // addresses to email pdf to
        $from        = "academy@cbn.com.au"; // address message is sent from
        $subject     = "INSIGHT Diagnostic Results"; // email subject
        $body        = "<p>Thank you for taking the time to complete the INSIGHT Diagnostic. Please see attached a copy of your results for your reference.</p>"; // email body
        $pdfLocation = dirname( __DIR__ ) . '/api/pdf/' . $hash . '.pdf'; // file location
        $pdfName     = $hash . '.pdf'; // pdf file name recipient will get
        $filetype    = "application/pdf"; // type

        // create headers and mime boundry
        $eol = PHP_EOL;
        $semi_rand     = md5(time());
        $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
        $headers       = "From: $from$eol" .
                         "MIME-Version: 1.0$eol" .
                         "Content-Type: multipart/mixed;$eol" .
                         " boundary=\"$mime_boundary\"";

        // add html message body
        $message = "--$mime_boundary$eol" .
            "Content-Type: text/html; charset=\"iso-8859-1\"$eol" .
            "Content-Transfer-Encoding: 7bit$eol$eol" .
            $body . $eol;

        // fetch pdf
        $file = fopen($pdfLocation, 'rb');
        $data = fread($file, filesize($pdfLocation));
        fclose($file);
        $pdf = chunk_split(base64_encode($data));

        // attach pdf to email
        $message .= "--$mime_boundary$eol" .
            "Content-Type: $filetype;$eol" .
            " name=\"Your-INSIGHT-Diagnostic-Results.pdf\"$eol" .
            "Content-Disposition: attachment;$eol" .
            " filename=\"Your-INSIGHT-Diagnostic-Results.pdf\"$eol" .
            "Content-Transfer-Encoding: base64$eol$eol" .
            $pdf . $eol .
            "--$mime_boundary--";

        // Send the email
        if($check = mail($to, $subject, $message, $headers)) {
            echo "The email was sent.";
        }
        else {
            echo "There was an error sending the mail.";
        }

        @unlink(dirname( __DIR__ ) . '/api/pdf/' . $hash . '.pdf');

    }
