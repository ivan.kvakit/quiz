# Instruction
## _First, install Wordpress_

## Next steps

**1. In the admin panel, activate the theme "twentytwentyone"**
**2. Add a menu item for uploading results**
```sh
in the file: wp-content/themes/twentytwentyone/functions.php
add the following at the end of the file:

add_action('admin_bar_menu', 'add_toolbar_items', 100);
function add_toolbar_items($admin_bar){
    $admin_bar->add_menu( array(
        'id'    => 'quiz',
        'title' => 'Quiz',
    ));

    // Submenus
    $admin_bar->add_menu( array(
        'parent' => 'quiz',
        'title' => 'Export', // Your submenu title
        'href'  => '/api/quiz_export.php', // URL
    ));
}
```

**3. Transfer the content of this repository/archive into the project using the same paths**

**4. Configuration**

```sh
in the file: helpers/QuizHelper.php
Change the constant to the current domain:
const HOST = 'https://insightignite.cbn.com.au/?pdf=1';
```
**5. Dompdf install**

```sh
in the folder: api/
run: composer install
```
