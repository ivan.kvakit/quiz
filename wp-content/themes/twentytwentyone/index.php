<?php
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    require_once dirname( __DIR__ ) . '/../../wp-load.php';
    require_once dirname( __DIR__ ) . '/../../helpers/QuizHelper.php';
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>INSIGHT Diagnostic</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css?v=<?=rand(1,1000)?>">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentytwentyone/quiz/table.css?v=<?=rand(1000,10000)?>">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentytwentyone/quiz/footer.css?v=<?=rand(1000,10000)?>">
    <link rel="shortcut icon" type="image/x-icon" href="/wp-content/themes/twentytwentyone/quiz/img/CBnet_favicon.png">
    <meta charset="utf-8">
</head>
<body>

<?php
    if(isset($_GET['pdf'])){
        global $wpdb;

        $quiz = $wpdb->get_results("SELECT * FROM " . QuizHelper::TABLE_USERS . " WHERE hash = '".$_GET['hash']."'");
        if(isset($quiz[0])){
            $quiz = (array)$quiz[0];
        }
        ?>

        <table border="0" style="border-bottom: 6px solid #1c0f2b;" width="100%">
            <tr>
                <td width="20%" height="auto"><img style="width: 100%;" class="logo" src="<?='data:image/png;base64,' . base64_encode(file_get_contents(dirname( __DIR__ ) . '/../../wp-content/themes/twentytwentyone/quiz/img/cbn-academy-logo.png'))?>" alt="CBN Academy Logo"></td>
                <!-- <td width="20%" height="auto"><img style="width: 100%;" class="logo" src="/wp-content/themes/twentytwentyone/quiz/img/cbn-academy-logo.png" alt="CBN Academy Logo"></td> -->
                <td width="80%" style="text-align: right;"><span style="padding: 50px 0; font-weight: bold; font-family: Verdana;font-size: 40px;line-height: 40px;letter-spacing: 0.03em;color: #1c0f2b;">INSIGHT Diagnostic</span></td>
            </tr>
        </table>

        <!-- Result Title Start -->
        <div class="endel-main-container" id="step3" style="font-family: Verdana,Geneva,sans-serif !important;" >

            <!-- Login title -->

            <div class="endel-container full result-title">
                <div class="wpb_wrapper">
                    <h1>Your results</h1>
                </div>
            </div>

            <!-- Result Title End-->

            <!-- Result Content Start -->
            <div class="endel-container full">
                <div class="result-content-left">
                    <p>Based on your answers, your INSIGHT usage has been identified as&nbsp;<span class="state">DEVELOPING</span>.<br>This means the following have been identified as areas of&nbsp;strength and areas of&nbsp;development. </p>

                    <table border="1" style="max-width: 100%;" align="center">
                        <thead>
                        <tr>
                            <th style="padding: 3px 15px; background-color: #1c0f2b; color: #fff;">Areas of development</th>
                            <th style="padding: 3px 15px; background-color: #1c0f2b; color: #fff;">Areas of strength</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                                $rows = json_decode($quiz['results'], 1);
                                if (!isset($rows['areas_of_development'])){
                                    $rows['areas_of_development'] = [];
                                }
                                if (!isset($rows['areas_of_strenght'])){
                                    $rows['areas_of_strenght'] = [];
                                }
                                $maxCount = count($rows['areas_of_development']) > count($rows['areas_of_strenght']) ? count($rows['areas_of_development']) : count($rows['areas_of_strenght']);

                                for ($i = 0; $i < $maxCount; $i++){
                                    echo "<tr>";
                                        echo "<td style='padding: 3px 15px; text-align: center;'>".(isset($rows['areas_of_strenght'][$i]) ? $rows['areas_of_strenght'][$i]['value'] : '')."</td>";
                                        echo "<td style='padding: 3px 15px; text-align: center;'>".(isset($rows['areas_of_development'][$i]) ? $rows['areas_of_development'][$i] : '')."</td>";
                                    echo "</tr>";
                                }
                            ?>
                        </tbody>

                    </table>
                    <p>For those areas that require development, please keep your eye out for upcoming workshops from the CBN Academy tailored to these topics.</p>
                </div>
<!--                <div class="result-content-right">-->
<!--                    <div class="test-block">-->

                        <?php
                            $point = ($quiz['point'] * 5);
                            $point = -77 + $point;
                        ?>

<!--                        <img class="test-score" src="/wp-content/themes/twentytwentyone/quiz/img/score.png" alt="">-->
<!--                        <img class="test-arrow" style="transform: rotate(deg);" src="/wp-content/themes/twentytwentyone/quiz/img/arrow.svg" alt="">-->

<!--                        <div class="arrowText"></div>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
            <!-- Result Content End -->
        </div>
        <!-- Result End -->
        <div class="page_break" style="page-break-before: always;"></div>
        <!-- Table Start -->
        <div class="endel-main-container" id="step2" style="padding: 15px; font-family: Verdana,Geneva,sans-serif !important;">

            <!-- Page title & description Start-->
            <div class="endel-container full result-title">
                <div class="wpb_wrapper">
                    <h1>Detailed answers</h1>
                    <!-- <h1>Welcome to the INSIGHT Diagnostic Tool</h1>
                    <p>Please rate how you currently use INSIGHT.  <br>Key points to note: it is important to be honest; no one, aside from the CBN Academy will see your results; and the information that you provide will be used to inform our training offering.</p> -->
                </div>
            </div>

            <!-- Page title & description End-->

            <style>
                .endel-row{
                    display: block;
                    clear: both;
                }
                .table-header .endel-row__item--1,
                .table-header .endel-row__item--2,
                .table-header .endel-row__item--3,
                .table-header .endel-row__item--4{
                    height: 58px;
                }
                .endel-row__item--1, .endel-row__item--2, .endel-row__item--3, .endel-row__item--4{
                    float: left;
                    width: 23%;
                    display: block;
                    flex: none;
                    height: 145px;
                    padding: 1%;
                }
                .endel-row__item--2:hover,
                .endel-row__item--2.active {
                	background-color: rgb(250, 91, 96);
                }

                .endel-row__item--3:hover,
                .endel-row__item--3.active {
                	background-color: rgb(254, 237, 68);
                }

                .endel-row__item--4:hover,
                .endel-row__item--4.active {
                	background-color: rgb(110, 216, 198);
                }
            </style>

            <!-- Table Start-->

            <div class="endel-container narrow" style="width: 100%;">

                <!-- Table Header Start -->

                <div class="endel-row table-header">
                    <row class="endel-row__item--1" style="background-color: #1c0f2b; color: white; text-align: center; ">

                    </row>
                    <row class="endel-row__item--2" style="background-color: #1c0f2b; color: white; text-align: center; ">
                        Take off <br> <small>(Developing)</small>
                    </row>
                    <row class="endel-row__item--3" style="background-color: #1c0f2b; color: white; text-align: center; ">
                        Fly <br> <small>(Functional)</small>
                    </row>
                    <row class="endel-row__item--4" style="background-color: #1c0f2b; color: white; text-align: center; ">
                        Soar <br> <small>(Strong)</small>
                    </row>
                </div>

                <!-- Table Header End -->

                <!-- Table Rows Start -->

                <?php
                    $index = 1;
                    if (in_array('Client Details', $rows['areas_of_development'])){
                        $index = 3;
                    }else{
                        for ($i = 0; $i < count($rows['areas_of_strenght']); $i++){
                            if ($rows['areas_of_strenght'][$i]['value'] == 'Client Details'){
                                $index = $rows['areas_of_strenght'][$i]['item'];
                            }
                        }
                    }
//                    $index = in_array('Client Details', $rows['areas_of_strenght']) ? 1 : (in_array('Client Details', $rows['areas_of_development']) ? 3 : 2);
                ?>

                <div class="endel-row even">
                    <row class="endel-row__item--1" style="background-color: #1c0f2b; color: white; text-align: left; ">
                        Client Details
                    </row>
                    <row class="endel-row__item--2 <?=$index == 1 ? 'active' : ''?>">
                        <div class="item-content">
                            I only add the mandatory client information when setting up a new client. (e.g name, address, phone number).
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3 <?=$index == 2 ? 'active' : ''?>">
                        <div class="item-content">
                            I try to add as much client information as possible when setting up a new client (e.g. website, ABN email, primary contact).
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4 <?=$index == 3 ? 'active' : ''?>">
                        <div class="item-content">
                            I add substantial client information when setting up a new client (e.g. source of business, standard industry code, area and client type).
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <?php
                    $index = 1;
                    if (in_array('Transactions', $rows['areas_of_development'])){
                        $index = 3;
                    }else{
                        for ($i = 0; $i < count($rows['areas_of_strenght']); $i++){
                            if ($rows['areas_of_strenght'][$i]['value'] == 'Transactions'){
                                $index = $rows['areas_of_strenght'][$i]['item'];
                            }
                        }
                    }
//                    $index = in_array('Transactions', $rows['areas_of_strenght']) ? 1 : (in_array('Transactions', $rows['areas_of_development']) ? 3 : 2);
                ?>

                <div class="endel-row odd">
                    <row class="endel-row__item--1" style="background-color: #1c0f2b; color: white; text-align: left; ">
                        Transactions
                    </row>
                    <row class="endel-row__item--2 <?=$index == 1 ? 'active' : ''?>">
                        <div class="item-content">
                            I <b>can’t</b> currently use INSIGHT to successfully complete transactions. This includes Sunrise, SCTP  and Manual processing, as required.
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3 <?=$index == 2 ? 'active' : ''?>">
                        <div class="item-content">
                            I can complete transactions in INSIGHT, however, need to refer to resources or seek support for transactions I don’t do daily.
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4 <?=$index == 3 ? 'active' : ''?>">
                        <div class="item-content">
                            I can complete all required transactions within INSIGHT.
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <?php
                    $index = 1;
                    if (in_array('SCTP', $rows['areas_of_development'])){
                        $index = 3;
                    }else{
                        for ($i = 0; $i < count($rows['areas_of_strenght']); $i++){
                            if ($rows['areas_of_strenght'][$i]['value'] == 'SCTP'){
                                $index = $rows['areas_of_strenght'][$i]['item'];
                            }
                        }
                    }
//                    $index = in_array('SCTP', $rows['areas_of_strenght']) ? 1 : (in_array('SCTP', $rows['areas_of_development']) ? 3 : 2);
                ?>

                <div class="endel-row even">
                    <row class="endel-row__item--1" style="background-color: #1c0f2b; color: white; text-align: left; ">
                        SCTP
                    </row>
                    <row class="endel-row__item--2 <?=$index == 1 ? 'active' : ''?>">
                        <div class="item-content">
                            I <b>don’t</b> currently use SCTP for available products.
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3 <?=$index == 2 ? 'active' : ''?>">
                        <div class="item-content">
                            I sometimes use SCTP for available products.
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4 <?=$index == 3 ? 'active' : ''?>">
                        <div class="item-content">
                            I consistently use SCTP when available to provide quotes and write business.
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <?php
                    $index = 1;
                    if (in_array('Notes and Document Management', $rows['areas_of_development'])){
                        $index = 3;
                    }else{
                        for ($i = 0; $i < count($rows['areas_of_strenght']); $i++){
                            if ($rows['areas_of_strenght'][$i]['value'] == 'Notes and Document Management'){
                                $index = $rows['areas_of_strenght'][$i]['item'];
                            }
                        }
                    }
//                    $index = in_array('Notes and Document Management', $rows['areas_of_strenght']) ? 1 : (in_array('Notes and Document Management', $rows['areas_of_development']) ? 3 : 2);
                ?>

                <div class="endel-row odd">
                    <row class="endel-row__item--1" style="background-color: #1c0f2b; color: white; text-align: left; ">
                        Notes and Document Management
                    </row>
                    <row class="endel-row__item--2 <?=$index == 1 ? 'active' : ''?>">
                        <div class="item-content">
                            I <b>don’t</b> consistently use either of these features to keep a detailed record of client interactions.
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3 <?=$index == 2 ? 'active' : ''?>">
                        <div class="item-content">
                            I effectively use some fields within these features to keep a detailed record of client interactions.
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4 <?=$index == 3 ? 'active' : ''?>">
                        <div class="item-content">
                            I consistently use all fields within these features to keep a detailed record of client interactions.
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <?php
                    $index = 1;
                    if (in_array('Tasks', $rows['areas_of_development'])){
                        $index = 3;
                    }else{
                        for ($i = 0; $i < count($rows['areas_of_strenght']); $i++){
                            if ($rows['areas_of_strenght'][$i]['value'] == 'Tasks'){
                                $index = $rows['areas_of_strenght'][$i]['item'];
                            }
                        }
                    }
//                    $index = in_array('Tasks', $rows['areas_of_strenght']) ? 1 : (in_array('Tasks', $rows['areas_of_development']) ? 3 : 2);
                ?>

                <div class="endel-row even">
                    <row class="endel-row__item--1" style="background-color: #1c0f2b; color: white; text-align: left; ">
                        Tasks
                    </row>
                    <row class="endel-row__item--2 <?=$index == 1 ? 'active' : ''?>">
                        <div class="item-content">
                            I <b>don’t</b> use this feature to schedule activities.
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3 <?=$index == 2 ? 'active' : ''?>">
                        <div class="item-content">
                            I sometimes use this feature to schedule activities.
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4 <?=$index == 3 ? 'active' : ''?>">
                        <div class="item-content">
                            I consistently use this feature to schedule activities, including leveraging the CBN branded templates.
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <?php
                    $index = 1;
                    if (in_array('Reporting', $rows['areas_of_development'])){
                        $index = 3;
                    }else{
                        for ($i = 0; $i < count($rows['areas_of_strenght']); $i++){
                            if ($rows['areas_of_strenght'][$i]['value'] == 'Reporting'){
                                $index = $rows['areas_of_strenght'][$i]['item'];
                            }
                        }
                    }
//                    $index = in_array('Reporting', $rows['areas_of_strenght']) ? 1 : (in_array('Reporting', $rows['areas_of_development']) ? 3 : 2);
                ?>

                <div class="endel-row odd">
                    <row class="endel-row__item--1" style="background-color: #1c0f2b; color: white; text-align: left; ">
                        Reporting
                    </row>
                    <row class="endel-row__item--2 <?=$index == 1 ? 'active' : ''?>">
                        <div class="item-content">
                            I <b>don’t</b> have a structured reporting regime and struggle running basic reports.
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3 <?=$index == 2 ? 'active' : ''?>">
                        <div class="item-content">
                            I am comfortable running the standard reports (including Renewal Report, Debtor Management and Unallocated Cash Reports) to manage my portfolio.
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4 <?=$index == 3 ? 'active' : ''?>">
                        <div class="item-content">
                            I consistently run complex reports (including Register of Business Written and Portfolio Analysis) to give me data on my portfolio.
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <?php
//                    echo "<pre>";
//                    var_dump(count($rows['areas_of_strenght']));
//                    var_dump($rows['areas_of_strenght']);
//                    die();
                    $index = 1;
                    if (in_array('Claims', $rows['areas_of_development'])){
                        $index = 3;
                    }else{
                        for ($i = 0; $i < count($rows['areas_of_strenght']); $i++){
                            if ($rows['areas_of_strenght'][$i]['value'] == 'Claims'){
                                $index = (int) $rows['areas_of_strenght'][$i]['item'];
                            }
                        }
                    }
//                    $index = in_array('Claims', $rows['areas_of_strenght']) ? 1 : (in_array('Claims', $rows['areas_of_development']) ? 3 : 2);
                ?>

                <div class="endel-row even">
                    <row class="endel-row__item--1" style="background-color: #1c0f2b; color: white; text-align: left; ">
                        Claims
                    </row>
                    <row class="endel-row__item--2 <?=$index == 1 ? 'active' : ''?>">
                        <div class="item-content">
                            I <b>don’t</b> enter claims into INSIGHT.
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3 <?=$index == 2 ? 'active' : ''?>">
                        <div class="item-content">
                            I add claims to INSIGHT, including basic details.
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4 <?=$index == 3 ? 'active' : ''?>">
                        <div class="item-content">
                            I add claims to INSIGHT, including detailed information.  In addition, I leverage claim tasks for file management and follow up.
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <?php
                    $index = 1;
                    if (in_array('Compliance Documents', $rows['areas_of_development'])){
                        $index = 3;
                    }else{
                        for ($i = 0; $i < count($rows['areas_of_strenght']); $i++){
                            if ($rows['areas_of_strenght'][$i]['value'] == 'Compliance Documents'){
                                $index = $rows['areas_of_strenght'][$i]['item'];
                            }
                        }
                    }
//                    $index = in_array('Compliance Documents', $rows['areas_of_strenght']) ? 1 : (in_array('Compliance Documents', $rows['areas_of_development']) ? 3 : 2);
                ?>

                <div class="endel-row odd">
                    <row class="endel-row__item--1" style="background-color: #1c0f2b; color: white; text-align: left; ">
                        Compliance Documents
                    </row>
                    <row class="endel-row__item--2 <?=$index == 1 ? 'active' : ''?>">
                        <div class="item-content">
                            I <b>don’t</b> currently or consistently use the templates and documents (including FSG, SOA, LOA, LARQ and LOE).
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3 <?=$index == 2 ? 'active' : ''?>">
                        <div class="item-content">
                            I sometimes use the templates and documents (including FSG, SOA, LOA, LARQ and LOE).
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4 <?=$index == 3 ? 'active' : ''?>">
                        <div class="item-content">
                            I consistently use the templates and documents (including FSG, SOA, LOA, LARQ and LOE).
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <?php
                    $index = 1;
                    if (in_array('Dashboard & To Do List', $rows['areas_of_development'])){
                        $index = 3;
                    }else{
                        for ($i = 0; $i < count($rows['areas_of_strenght']); $i++){
                            if ($rows['areas_of_strenght'][$i]['value'] == 'Dashboard & To Do List'){
                                $index = $rows['areas_of_strenght'][$i]['item'];
                            }
                        }
                    }
//                    $index = in_array('Dashboard & To Do List', $rows['areas_of_strenght']) ? 1 : (in_array('Dashboard & To Do List', $rows['areas_of_development']) ? 3 : 2);
                ?>

                <div class="endel-row even">
                    <row class="endel-row__item--1" style="background-color: #1c0f2b; color: white; text-align: left; ">
                        Dashboard & To Do List
                    </row>
                    <row class="endel-row__item--2 <?=$index == 1 ? 'active' : ''?>">
                        <div class="item-content">
                            I <b>don’t</b> currently or consistently use Dashboards and/or My Lists to manage activities.
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3 <?=$index == 2 ? 'active' : ''?>">
                        <div class="item-content">
                            I inconsistently use Dashboards and/or My Lists to manage activities.
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4 <?=$index == 3 ? 'active' : ''?>">
                        <div class="item-content">
                            I consistently use Dashboards and/or My Lists to manage outstanding activities.
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <?php
                    $index = 1;
                    if (in_array('Resources – User Guides and Videos', $rows['areas_of_development'])){
                        $index = 3;
                    }else{
                        for ($i = 0; $i < count($rows['areas_of_strenght']); $i++){
                            if ($rows['areas_of_strenght'][$i]['value'] == 'Resources – User Guides and Videos'){
                                $index = $rows['areas_of_strenght'][$i]['item'];
                            }
                        }
                    }
//                    $index = in_array('Resources – User Guides and Videos', $rows['areas_of_strenght']) ? 1 : (in_array('Resources – User Guides and Videos', $rows['areas_of_development']) ? 3 : 2);
                ?>

                <div class="endel-row odd">
                    <row class="endel-row__item--1" style="background-color: #1c0f2b; color: white; text-align: left; ">
                        Resources – User Guides and Videos
                    </row>
                    <row class="endel-row__item--2 <?=$index == 1 ? 'active' : ''?>">
                        <div class="item-content">
                            I <b>don’t</b> currently or consistently use the User Guides or Videos.
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3 <?=$index == 2 ? 'active' : ''?>">
                        <div class="item-content">
                            I inconsistently use the User Guides or Videos.
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4 <?=$index == 3 ? 'active' : ''?>">
                        <div class="item-content">
                            I consistently, when required, use the User Guides or Videos.
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>
                <!-- Table Rows End -->
            </div>
            <!-- Table End -->
        </div>
        <!-- Table End -->

        <?php
    }else{
        ?>

        <header>
            <div class="endel-container full">
                <div class="logo_wrapper">
                    <img class="logo" src="/wp-content/themes/twentytwentyone/quiz/img/cbn-academy-logo.svg" alt="CBN Academy Logo">
                    <span class="logo-title">INSIGHT Diagnostic</span>
                </div>


            </div>

        </header>

        <!-- Login Start -->
        <div class="endel-main-container dnone" id="step1">

            <!-- Login title -->

            <div class="endel-container full">
                <div class="wpb_wrapper">
                    <h1>Please fill in your details below to begin</h1>
                </div>
            </div>

            <!-- Ligin title End-->


            <!-- Form Start -->

            <div class="endel-container narrow">
                <div class="login form">
                    <div class="form__row half">
                        <div class="form__item">
                            <label class="label-left">
                                First Name :
                                <span class="require-red">*</span>
                            </label>
                            <input id="first-name" type="text">
                        </div>
                        <div class="form__item">
                            <label class="label-left">
                                Last Name :
                                <span class="require-red">*</span>
                            </label>
                            <input id="last-name" type="text">
                        </div>
                    </div>
                    <div class="form__row half">
                        <div class="form__item">
                            <label class="label-left">
                                Preferred Email Address :
                                <span class="require-red">*</span>
                            </label>
                            <input id="email" type="text">
                        </div>
                        <div class="form__item">
                            <label class="label-left">
                                How long have you been using INSIGHT? :
                                <span class="require-red">*</span>
                            </label>
                            <div class="endel-dropdown">
                                <select name="menu-dropdown">
                                    <option value="less3month">&#60; 3 months</option>
                                    <option value="less1year">&#60; 1 year</option>
                                    <option value="1-2years">1-2 years</option>
                                    <option value="more2years">2+ years</option>
                                </select>
                                <div class="arrow-down"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Form End -->

            <!-- Button Start -->

            <div class="endel-container full" id="btn">
                <a class="endel-button">
                    SAVE AND NEXT
                </a>
            </div>

            <!-- Button End -->

        </div>
        <!-- Login End -->

        <!-- Table Start -->
        <div class="endel-main-container dnone" id="step2">

            <!-- Page title & description Start-->

            <div class="endel-container full">
                <div class="wpb_wrapper">
                    <h1>Welcome to the INSIGHT Diagnostic Tool</h1>
                    <p>Please rate how you currently use INSIGHT.  <br>Key points to note: it is important to be honest; no one, aside from the CBN Academy will see your results; and the information that you provide will be used to inform our training offering.</p>
                </div>
            </div>

            <!-- Page title & description End-->


            <!-- Table Start-->

            <div class="endel-container narrow">

                <!-- Table Header Start -->

                <div class="endel-row table-header">
                    <row class="endel-row__item--1">

                    </row>
                    <row class="endel-row__item--2">
                        Take off <br> <small>(Developing)</small>
                    </row>
                    <row class="endel-row__item--3">
                        Fly <br> <small>(Functional)</small>
                    </row>
                    <row class="endel-row__item--4">
                        Soar <br> <small>(Strong)</small>
                    </row>
                </div>

                <!-- Table Header End -->

                <!-- Table Rows Start -->

                <div class="endel-row even">
                    <row class="endel-row__item--1">
                        Client Details
                    </row>
                    <row class="endel-row__item--2">
                        <div class="item-content">
                            I only add the mandatory client information when setting up a new client. (e.g name, address, phone number).
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3">
                        <div class="item-content">
                            I try to add as much client information as possible when setting up a new client (e.g. website, ABN email, primary contact).
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4">
                        <div class="item-content">
                            I add substantial client information when setting up a new client (e.g. source of business, standard industry code, area and client type).
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <div class="endel-row odd">
                    <row class="endel-row__item--1">
                        Transactions
                    </row>
                    <row class="endel-row__item--2">
                        <div class="item-content">
                            I <b>can’t</b> currently use INSIGHT to successfully complete transactions. This includes Sunrise, SCTP  and Manual processing, as required.
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3">
                        <div class="item-content">
                            I can complete transactions in INSIGHT, however, need to refer to resources or seek support for transactions I don’t do daily.
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4">
                        <div class="item-content">
                            I can complete all required transactions within INSIGHT.
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <div class="endel-row even">
                    <row class="endel-row__item--1">
                        SCTP
                    </row>
                    <row class="endel-row__item--2">
                        <div class="item-content">
                            I <b>don’t</b> currently use SCTP for available products.
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3">
                        <div class="item-content">
                            I sometimes use SCTP for available products.
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4">
                        <div class="item-content">
                            I consistently use SCTP when available to provide quotes and write business.
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <div class="endel-row odd">
                    <row class="endel-row__item--1">
                        Notes and Document Management
                    </row>
                    <row class="endel-row__item--2">
                        <div class="item-content">
                            I <b>don’t</b> consistently use either of these features to keep a detailed record of client interactions.
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3">
                        <div class="item-content">
                            I effectively use some fields within these features to keep a detailed record of client interactions.
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4">
                        <div class="item-content">
                            I consistently use all fields within these features to keep a detailed record of client interactions.
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <div class="endel-row even">
                    <row class="endel-row__item--1">
                        Tasks
                    </row>
                    <row class="endel-row__item--2">
                        <div class="item-content">
                            I <b>don’t</b> use this feature to schedule activities.
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3">
                        <div class="item-content">
                            I sometimes use this feature to schedule activities.
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4">
                        <div class="item-content">
                            I consistently use this feature to schedule activities, including leveraging the CBN branded templates.
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <div class="endel-row odd">
                    <row class="endel-row__item--1">
                        Reporting
                    </row>
                    <row class="endel-row__item--2">
                        <div class="item-content">
                            I <b>don’t</b> have a structured reporting regime and struggle running basic reports.
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3">
                        <div class="item-content">
                            I am comfortable running the standard reports (including Renewal Report, Debtor Management and Unallocated Cash Reports) to manage my portfolio.
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4">
                        <div class="item-content">
                            I consistently run complex reports (including Register of Business Written and Portfolio Analysis) to give me data on my portfolio.
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <div class="endel-row even">
                    <row class="endel-row__item--1">
                        Claims
                    </row>
                    <row class="endel-row__item--2">
                        <div class="item-content">
                            I <b>don’t</b> enter claims into INSIGHT.
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3">
                        <div class="item-content">
                            I add claims to INSIGHT, including basic details.
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4">
                        <div class="item-content">
                            I add claims to INSIGHT, including detailed information.  In addition, I leverage claim tasks for file management and follow up.
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <div class="endel-row odd">
                    <row class="endel-row__item--1">
                        Compliance Documents
                    </row>
                    <row class="endel-row__item--2">
                        <div class="item-content">
                            I <b>don’t</b> currently or consistently use the templates and documents (including FSG, SOA, LOA, LARQ and LOE).
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3">
                        <div class="item-content">
                            I sometimes use the templates and documents (including FSG, SOA, LOA, LARQ and LOE).
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4">
                        <div class="item-content">
                            I consistently use the templates and documents (including FSG, SOA, LOA, LARQ and LOE).
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <div class="endel-row even">
                    <row class="endel-row__item--1">
                        Dashboard & To Do List
                    </row>
                    <row class="endel-row__item--2">
                        <div class="item-content">
                            I <b>don’t</b> currently or consistently use Dashboards and/or My Lists to manage activities.
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3">
                        <div class="item-content">
                            I inconsistently use Dashboards and/or My Lists to manage activities.
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4">
                        <div class="item-content">
                            I consistently use Dashboards and/or My Lists to manage outstanding activities.
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <div class="endel-row odd">
                    <row class="endel-row__item--1">
                        Resources – User Guides and Videos
                    </row>
                    <row class="endel-row__item--2">
                        <div class="item-content">
                            I <b>don’t</b> currently or consistently use the User Guides or Videos.
                        </div>
                        <!-- <div class="header-title-responsive red">Take off / Developing</div> -->
                    </row>
                    <row class="endel-row__item--3">
                        <div class="item-content">
                            I inconsistently use the User Guides or Videos.
                        </div>
                        <!-- <div class="header-title-responsive yellow">Fly / Functional</div> -->
                    </row>
                    <row class="endel-row__item--4">
                        <div class="item-content">
                            I consistently, when required, use the User Guides or Videos.
                        </div>
                        <!-- <div class="header-title-responsive green">Soar / Strong</div> -->
                    </row>
                </div>

                <!-- Table Rows End -->


            </div>

            <!-- Table End -->

            <!-- Button Start -->

            <div class="endel-container full" id="btn">
                <a class="endel-button">
                    SAVE AND NEXT
                </a>
            </div>

            <!-- Button End -->
        </div>
        <!-- Table End -->

        <!-- Result Title Start -->
        <div class="endel-main-container dnone" id="step3">

            <!-- Login title -->

            <div class="endel-container full result-title">
                <div class="wpb_wrapper">
                    <h1>Your results</h1>
                </div>
            </div>

            <!-- Result Title End-->

            <!-- Result Content Start -->
            <div class="endel-container full">
                <div class="result-content-left">
                    <p>Based on your answers, your INSIGHT usage has been identified as&nbsp;<span class="state">DEVELOPING</span>.<br>This means the following have been identified as areas of&nbsp;strength and areas of&nbsp;development. </p>

                    <table>
                        <thead>
                        <tr>
                            <th>Areas of development</th>
                            <th>Areas of strength</th>
                        </tr>
                        </thead>
                        <tbody>
                        <!-- <tr>
                            <td> Testimonials</td>
                            <td> Notes and Document Management</td>
                        </tr>
                        <tr>
                            <td> SCTP</td>
                            <td> fResources – User Guides and Videos</td>
                        </tr>
                        <tr>
                            <td> sd ad dafs fa df asdf</td>
                            <td> fadsf asd dfa sdf sdf f</td>
                        </tr>
                        <tr>
                            <td> sd ad dafs fa df asdf</td>
                        </tr>
                        <tr>
                            <td> sd ad dafs fa df asdf</td>
                        </tr> -->
                        </tbody>

                    </table>
                    <p>For those areas that require development, please keep your eye out for upcoming workshops from the CBN Academy tailored to these topics.</p>
                </div>
                <div class="result-content-right">
                    <div class="test-block">
                        <img class="test-score" src="/wp-content/themes/twentytwentyone/quiz/img/score.png" alt="">
                        <img class="test-arrow" src="/wp-content/themes/twentytwentyone/quiz/img/arrow.svg" alt="">

                        <div class="arrowText"></div>
                    </div>
                </div>
            </div>
            <!-- Result Content End -->

            <!-- Button Start -->
            <div class="endel-container full" id="btn">
                <a class="endel-button">
                    SAVE AND NEXT
                </a>
            </div>

            <!-- Button End -->
        </div>
        <!-- Result End -->

        <!-- Survey Start -->
        <div class="endel-main-container dnone" id="step4">

            <!-- Login title -->

            <div class="endel-container full">
                <div class="wpb_wrapper">
                    <h1>Please fill in the following questions to receive a PDF of your results via email</h1>
                    <!-- <p>Please rate how you currently use INSIGHT.  Key points to note: it is important to be honest; no one, aside from the CBN Academy will see your results; and the information that you provide will be used to inform our training offering.</p> -->
                </div>
            </div>

            <!-- Survey title End-->

            <!-- Form Start -->

            <div class="endel-container narrow">
                <div class="survey_form">
                    <label>What are your top 3 likes about INSIGHT?</label>
                    <div class="form__row">
                        <div class="form__item fw">
                            <label class="label-left">
                                Like :
                                <span class="require-red">*</span>
                            </label>
                            <input class="input-col" id="Like-1" type="text">
                        </div>
                        <div class="form__item fw">
                            <label class="label-left">
                                Like :
                            </label>
                            <input class="input-col" id="Like-2" type="text">
                        </div>
                        <div class="form__item fw">
                            <label class="label-left">
                                Like :
                            </label>
                            <input class="input-col" id="Like-3" type="text">
                        </div>
                    </div>
                    <label>What are your top 3 dislikes about INSIGHT?</label>
                    <div class="form__row">
                        <div class="form__item fw">
                            <label class="label-left">
                                Dislike :
                                <span class="require-red">*</span>
                            </label>
                            <input class="input-col" id="Dislike-1" type="text">
                        </div>
                        <div class="form__item fw">
                            <label class="label-left">
                                Dislike :
                            </label>
                            <input class="input-col" id="Dislike-2" type="text">
                        </div>
                        <div class="form__item fw">
                            <label class="label-left">
                                Dislike :
                            </label>
                            <input class="input-col" id="Dislike-3" type="text">
                        </div>
                    </div>


                    <div class="form__row fw">
                        <div class="form__item">
                            <label class="label-left">
                                What enchancements would you like to see on INSIGHT? :
                                <span class="require-red">*</span>
                            </label>
                            <textarea id="like-to-see" type="text"></textarea>
                        </div>





                    </div>
                </div>
            </div>

            <!-- Form End -->

            <!-- Button Start -->

            <div class="endel-container full" id="btn">
                <a class="endel-button">
                    EMAIL ME MY RESULTS
                </a>
            </div>

            <!-- Button End -->

        </div>
        <!-- Survey End -->

        <div class="endel-main-container dnone" id="step5">

            <!-- Login title -->

            <div class="endel-container full">
                <div class="wpb_wrapper">
                    <h1>You have successfully completed the INSIGHT Diagnostic, and your results have been sent to your email: <span style="font-family: Overpass-SemiBold; font-size: 36px; font-weight: 500; line-height: 1.25em; margin-bottom: 26px; color: #303030; text-decoration: underline;" id="user-email"></span></h1>
                    <!-- <p>Please rate how you currently use INSIGHT.  Key points to note: it is important to be honest; no one, aside from the CBN Academy will see your results; and the information that you provide will be used to inform our training offering.</p> -->
                </div>
            </div>

            <!-- Survey title End-->
        </div>

        <!-- Result Start -->
        <!-- Main Content End-->

        <!-- Footer Start  -->
        <footer>
            <div class="endel-container-fluid main-footer">
                <div class="endel-container-footer">
                    <div class="endel-footer-container">
                        <div class="footer-logo-bg"></div>
                        <img class="footer-logo" src="/wp-content/themes/twentytwentyone/quiz/img/CBN_Logo_White.svg" alt="">

                        <div class="endel-footer-item">
                            Medbourne Office<br>
                            Level 9, North Tover,<br>
                            459 Collins St<br>
                            Medbourne VIC 3000<br>
                            <br>
                            Perth Office<br>
                            Level 5, 28 The<br>
                            Esplanade<br>
                            Perth, Wa 6000<br>
                        </div>
                        <div class="endel-footer-item">
                            info@cbnet.com.au <br>
                            1300 905 577
                        </div>
                        <div class="endel-footer-item endel-footer-item-double">
                            <a target="_blank" href="https://www.facebook.com/cbrokernetwork/"><i class="fa fa-facebook"></i></a>
                            <a target="_blank" href="https://www.linkedin.com/company/community-broker-network"><i class="fa fa-linkedin"></i></a>
                        </div>
                        <div class="endel-footer-item">
                            Empowering<br>
                            our network
                        </div>
                    </div>
                </div>
            </div>

            <div class="endel-container-fluid sub-footer">
                <div class="endel-container-footer">
                    <div class="endel-container-subfooter-text">
                        <strong>Community Broker Network Pty Ltd | ABN 60 096 916 184 | AFSL 233750</strong><br>
                        The information contained in this document is general and is not intended to serve as advice. It does not take into account the objectives, financial situation or needs of any particular person.
                    </div>
                    <div class="endel-container-footer-links">
                        <ul id="menu-menu-footer" class="menu"><li id="menu-item-1318" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1318"><a href="https://gateway.nasinsurance.com.au/">Broker Portal</a></li>
                            <li id="menu-item-664" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-664"><a target="_blank" rel="noopener" href="https://www.cbnet.com.au/wp-content/uploads/2022/01/Community-Broker-Network-FSG-January-2022-1.pdf">Financial Services Guide</a></li>
                            <li id="menu-item-685" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-685"><a href="https://www.cbnet.com.au/privacy/">Privacy Policy</a></li>
                            <li id="menu-item-578" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-578"><a target="_blank" rel="noopener" href="https://www.niba.com.au/codeofpractice/index.cfm">Insurance Brokers Code of Practice</a></li>
                            <li id="menu-item-686" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-686"><a href="https://www.cbnet.com.au/complaints-disputes-handling/">Complaints &amp; Disputes Handling</a></li>
                            <li id="menu-item-944" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-944"><a href="https://download.teamviewer.com/download/TeamViewerQS.exe">Remote PC Support</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->

        <?php
    }
?>

<script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
<script src="/wp-content/themes/twentytwentyone/quiz/main.js?v=<?=rand(1,2000)?>"></script>

</body>
