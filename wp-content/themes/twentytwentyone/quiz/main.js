let storage = {};

try {
    if(typeof localStorage['quiz'] !== 'undefined'){
        storage = JSON.parse(localStorage['quiz']);
    }
} catch (e) {
    storage = {};
}

setStepValues(1);
$('#step1').slideDown(1000);

scrollToTop();

let area = {
    first: [],
    second: [],
};

$('.endel-button').click(function() {

    if($('#step1').is(':visible')){
        checkInputs('#step1');
        if(!$('#step1 .needTiFill').length){
            setToLocalStorage({
                firstName: $('#first-name').val().trim(),
                lastName: $('#last-name').val().trim(),
                email: $('#email').val().trim(),
                howLong: $('[name="menu-dropdown"] option:selected').text().trim(),
            });

            setStepValues(2);

            $('#step1').slideUp(1000);
            $('#step2').slideDown(1000);
        }
        scrollToTop();
    }else if($('#step2').is(':visible')){
        if($('#step2 .active').length == 10){

            let rows = $('#step2 .endel-row');
            area = {
                first: [],
                second: [],
            };

            let state = area.first.length > area.second.length ? 'TAKE OFF' : 'SOAR';
            let max = area.first.length > area.second.length ? area.first.length : area.second.length;

            if((10 - (area.first.length + area.second.length)) > max){
                state = 'FLY';
            }

            $('.state').text(state);

            for (var i = 1; i < rows.length; i++) {
                if($(rows[i]).find('.endel-row__item--2').hasClass('active')){
                    let obj = {};
                    obj.value = $(rows[i]).find('.endel-row__item--1').text().trim();
                    obj.item = 1;
                    area.first.push(obj);
                }else if($(rows[i]).find('.endel-row__item--4').hasClass('active')){
                    area.second.push($(rows[i]).find('.endel-row__item--1').text().trim());
                }else if ($(rows[i]).find('.endel-row__item--3').hasClass('active')){
                    let obj = {};
                    obj.value = $(rows[i]).find('.endel-row__item--1').text().trim();
                    obj.item = 2;
                    area.first.push(obj);
                }
            }

            let maxSize = area.first.length > area.second.length ? area.first.length : area.second.length;

            let tbodyHtml = '';

            for (var i = 0; i < maxSize; i++) {
                tbodyHtml += '<tr>';
                    tbodyHtml += (typeof area.first[i] !== 'undefined') ? '<td>'+area.first[i].value+'</td>' : '<td></td>';
                    tbodyHtml += (typeof area.second[i] !== 'undefined') ? '<td>'+area.second[i]+'</td>' : '<td></td>';
                tbodyHtml += '</tr>';
            }
            setToLocalStorage({
                area: area,
            });

            $('.result-content-left tbody').html(tbodyHtml);

            setStepValues(3);

            $('#step2').slideUp(1000);
            $('#step3').slideDown(1000);
        }
        scrollToTop();
    }else if($('#step3').is(':visible')){
        setStepValues(4);
        $('#step3').slideUp(1000);
        $('#step4').slideDown(1000);
        scrollToTop();
    }else if($('#step4').is(':visible')){
        checkInputs('#step4');

        if(!$('#step4 .needTiFill').length){
            let likes = '';
            let dislikes = '';

            if($('#Like-1').val().trim() !== ''){
                likes = $('#Like-1').val().trim();
            }

            if($('#Like-2').val().trim() !== ''){
                likes += ', ' + $('#Like-2').val().trim();
            }

            if($('#Like-3').val().trim() !== ''){
                likes += ', ' + $('#Like-3').val().trim();
            }

            if($('#Dislike-1').val().trim() !== ''){
                dislikes = $('#Dislike-1').val().trim();
            }

            if($('#Dislike-2').val().trim() !== ''){
                dislikes += ', ' + $('#Dislike-2').val().trim();
            }

            if($('#Dislike-3').val().trim() !== ''){
                dislikes += ', ' + $('#Dislike-3').val().trim();
            }

            setToLocalStorage({
                likes: likes,
                dislikes: dislikes,
                likeToSee: $('#like-to-see').val(),
            });

            saveToServer();

            $('#user-email').text(storage.email);
            $('#step4').slideUp(1000);
            $('#step5').slideDown(1000);
        }

        scrollToTop();
    }

});

function scrollToTop() {
    window.scrollTo({
        top: 0,
        behavior: "smooth"
    });
}

function checkInputs(selector) {

    $(selector + ' .needTiFill').removeClass('needTiFill');

    $(selector + ' input').each(function() {
        let val = $(this).val();
        val = val.trim();
        if(val === ''){
            $(this).addClass('needTiFill');
        }
    });

    $(selector + ' select').each(function() {
        let val = $(this).val();
        val = val.trim();
        if(val === ''){
            $(this).addClass('needTiFill');
        }
    });

    $(selector + ' textarea').each(function() {
        let val = $(this).val();
        val = val.trim();
        if(val === ''){
            $(this).addClass('needTiFill');
        }
    });

    $("#Like-2, #Like-3, #Dislike-2, #Dislike-3").removeClass('needTiFill');

}

$('.endel-row__item--2, .endel-row__item--3, .endel-row__item--4').click(function() {
    let parent = $(this).closest('.endel-row');
    let elements = $(parent).find('.endel-row__item--2, .endel-row__item--3, .endel-row__item--4');

    elements.each(function() {
        $(this).removeClass('active');
    });

    $(this).addClass('active');
});

function setToLocalStorage(data) {
    console.log(data)
    try {
        if(typeof localStorage['quiz'] !== 'undefined'){
            storage = JSON.parse(localStorage['quiz']);
        }
    } catch (e) {
        storage = {};
    }

    for(let key in data){
        storage[key] = data[key];
    }

    localStorage['quiz'] = JSON.stringify(storage);
}

function setStepValues(stepLoad) {
    if(stepLoad == 1){
        $('#first-name').val(typeof storage.firstName !== 'undefined' ? storage.firstName : '');
        $('#last-name').val(typeof storage.lastName !== 'undefined' ? storage.lastName : '');
        $('#email').val(typeof storage.email !== 'undefined' ? storage.email : '');

        $('[name="menu-dropdown"] option').prop('selected', false);

        if(typeof storage.howLong !== 'undefined'){
            $('[name="menu-dropdown"] option').each(function(){
                if($(this).text().trim() == storage.howLong){
                    $(this).prop('selected', true);
                }
            });
        }
    }else if(stepLoad == 2){
        if(typeof storage.area !== 'undefined'){
            if(typeof storage.area.first !== 'undefined' && storage.area.first.length !== 0 ||
                typeof storage.area.second !== 'undefined' && storage.area.second.length !== 0){

                if(typeof storage.area.first !== 'undefined'){
                    for (var i = 0; i < storage.area.first.length; i++) {
                        $('.endel-row__item--1').each(function(){
                            if($(this).text().trim() == storage.area.first[i].value){
                                let parent = $(this).closest('.endel-row');
                                if (storage.area.first[i].item == 1){
                                    $(parent).find('.endel-row__item--2').addClass('active');
                                }else{
                                    $(parent).find('.endel-row__item--3').addClass('active');
                                }
                            }
                        });
                    }
                }

                if(typeof storage.area.second !== 'undefined'){
                    for (var i = 0; i < storage.area.second.length; i++) {
                        $('.endel-row__item--1').each(function(){
                            if($(this).text().trim() == storage.area.second[i]){
                                let parent = $(this).closest('.endel-row');
                                $(parent).find('.endel-row__item--4').addClass('active');
                            }
                        });
                    }
                }

                $('#step2 .endel-row').not(':first').each(function(){
                    if($(this).find('.active').length == 0){
                        $(this).find('.endel-row__item--3').addClass('active');
                    }
                });
            }
        }
    }else if(stepLoad == 3){
        let points = 0;
        for (let k = 0; k < storage.area.first.length; k++){
            if (storage.area.first[k].item == 1){
                points++;
            }else{
                points += 2;
            }
        }
        // points += (storage.area.first.length);
        points += (storage.area.second.length * 3);
        points += ((10 - (storage.area.second.length + storage.area.first.length)) * 2);
        setToLocalStorage({
            points: points
        });

        setTimeout(setAnimation, 1000, points);
        // setAnimation(points);
        // console.log(points);
    }else if(stepLoad == 4){
        if(typeof storage.likes !== 'undefined') {
            if (storage.likes.trim() !== '') {
                let likes = storage.likes.split(',');
                $('#Like-1').val(typeof likes[0] !== 'undefined' ? likes[0] : '');
                $('#Like-2').val(typeof likes[1] !== 'undefined' ? likes[1] : '');
                $('#Like-3').val(typeof likes[2] !== 'undefined' ? likes[2] : '');
            }
        }

        if(typeof storage.dislikes !== 'undefined') {
            if (storage.dislikes.trim() !== '') {
                let dislikes = storage.dislikes.split(',');
                $('#Dislike-1').val(typeof dislikes[0] !== 'undefined' ? dislikes[0] : '');
                $('#Dislike-2').val(typeof dislikes[1] !== 'undefined' ? dislikes[1] : '');
                $('#Dislike-3').val(typeof dislikes[2] !== 'undefined' ? dislikes[2] : '');
            }
        }

        $('#like-to-see').val(typeof storage.likeToSee !== 'undefined' ? storage.likeToSee : '');
    }
}

function setAnimation(point){
    // -77 -24
    // -24 24
    // 24 77

    $('.arrowText').text(point);

    if(point <= 16){
        point = (point * 3.3125);
    }else if(point <= 23){
        point = (point * 4.3478);
    }else{
        point = (point * 5);
    }

    point = -77 + point;

    let style = '';

    style += '@keyframes arrowanimation {from {transform: rotate(-77deg)} to {transform: rotate('+point+'deg)}}';
    style += '.test-arrow {animation: arrowanimation 2s ease-out both;}';

    $('body').append('<style>'+style+'</style>');
}

function saveToServer(){
    $.ajax({
        url: '/api/save.php',
        type: 'POST',
        data: {
            quiz: JSON.parse(localStorage['quiz']),
            area
        },
        success: function (response){
            localStorage['quiz'] = '';
        }
    });
}
