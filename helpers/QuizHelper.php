<?php

    global $wpdb;

    class QuizHelper{

        const TABLE_USERS = 'quiz_users';
        const HOST = 'https://insightignite.cbn.com.au/?pdf=1';

        public static function getDbName(){
            echo DB_NAME;
        }

        public static function checkTables(){
            global $wpdb;
            $tables = $wpdb->get_results("SHOW TABLES FROM wp");
            $tabs = [];

            foreach ($tables as $tableName){
                $tabs[] = ((array)$tableName)["Tables_in_wp"];
            }

            if(!in_array(self::TABLE_USERS, $tabs)){
                self::createUsersTable();
            }
        }

        public static function createUsersTable(){
            global $wpdb;
            $sql = "
                create table ".self::TABLE_USERS."
                (
                    id                     int auto_increment,
                    first_name             varchar(50)                         null,
                    last_name              varchar(50)                         null,
                    email                  varchar(75)                         null,
                    using_insight          varchar(30)                         null,
                    hash                   varchar(32)                         null,
                    likes_about_insight    text                                null,
                    dislikes_about_insight text                                null,
                    enhancements_insight   text                                null,
                    point                  int                                 null,
                    results                text                                null,
                    created                timestamp default current_timestamp null,
                    constraint quiz_users_pk
                        primary key (id)
                )
            ";
            $wpdb->get_results($sql);
        }

        public static function exportResults(){
            error_reporting(E_ALL);
            ini_set("display_errors", 1);

            global $wpdb;
            $res = $wpdb->get_results("SELECT * FROM " . self::TABLE_USERS);

            foreach ($res as $k => $re) {
                $res[$k] = (array)$re;
            }
            return $res;
        }

        public static function save($data){
            error_reporting(E_ALL);
            ini_set("display_errors", 1);

            global $wpdb;

            $res = $wpdb->get_results("SELECT * FROM " . self::TABLE_USERS . " WHERE hash = '".$data['hash']."'");

            if(!count($res)){
                $wpdb->insert(self::TABLE_USERS,$data);
            }
        }

    }
